# generates 1/5-fold validation scores 
#    (i.e. makes a 4-to-1 split of the data, trains a model on the 4/5 part, 
#    and predicts scores for the 1/5 part)
# output: a pickled dataframe with columns: 'actual' score, 'predicted' score, 'supp'

from stackrecommender import Recommender
from stacksite import StackSite
import numpy as np
import pandas as pd
from sklearn.cross_validation import KFold
import warnings
import pickle as pkl
import sys, time

warnings.filterwarnings("ignore")

if __name__ == '__main__':

    start_time = time.time()

    site_name = 'worldbuilding.stackexchange.com'

    print "\n  = 1/5-Fold Validation ="
    print "\nGenerating dataframes from %s." % site_name
    sys.stdout.flush()

    site = StackSite(site_name)
    df_dict = site.generate_dfs()

    df_names = ['questions', 'comments']
    df_lengths = {name:len(site.df(name)) for name in df_names}

    # generate some 5-folds
    n_folds = 5
    folds = {name:KFold(df_lengths[name], n_folds=n_folds) for name in df_names}

    print "Splitting the dataframes."
    sys.stdout.flush()

	# generate the train/test dataframes
    train_dfs = {name:[] for name in df_names}
    test_dfs = {name:[] for name in df_names}
    for name in df_names:
	    for test, train in folds[name]:
	        train_dfs[name].append(site.df(name).ix[train])
	        test_dfs[name].append(site.df(name).ix[test])

	# split answers_df according to the train/test split of questions_d
    train_dfs['answers'] = range(n_folds)
    test_dfs['answers'] = range(n_folds)
    answers_df = site.df('answers').copy()

    for j in range(n_folds):
	    train_qids = set(train_dfs['questions'][j].index.unique()) # indices are already unique
	    test_qids = set(test_dfs['questions'][j].index.unique()) # but df.index is mutable and not hashable
	    train_dfs['answers'][j] = answers_df[answers_df.parent_id.isin(train_qids)]
	    test_dfs['answers'][j] = answers_df[answers_df.parent_id.isin(test_qids)]

    print "Packing the dataframes for the recommender."
    sys.stdout.flush()

	# pack the training sets into dictionaries of dataframes for the recommender
    df_names.append('answers')
    train_df_dicts = range(n_folds)
    for j in range(n_folds):
	    train_df_dicts[j] = {name:train_dfs[name][j] for name in df_names}
	    train_df_dicts[j]['tags'] = site.df('tags')
	    train_df_dicts[j]['users'] = site.df('users')

	# users and scores are lists of users/scores lists (one list for each fold)
	# question_ids is a list of lists of the corresponding question_ids
	# questions is a list of lists of questions (one list for each fold)

    users = [answers_df.user_id for answers_df in test_dfs['answers']]
    scores = [answers_df.score for answers_df in test_dfs['answers']]
    question_ids = [answers_df.parent_id for answers_df in test_dfs['answers']]
    questions = range(n_folds)
    for j in range(n_folds):
	    questions[j] = [test_dfs['questions'][j][['title','question','tags']].ix[qid] for qid in question_ids[j]]
	    
	# make a list of dataframes
    validate_dfs = range(n_folds)
    for j in range(n_folds):
	    validate_dfs[j] = pd.DataFrame(data=questions[j], index=question_ids[j])
	    validate_dfs[j]['user_id'] = pd.Series(data=users[j].values, index=question_ids[j])
	    validate_dfs[j]['score'] = pd.Series(data=scores[j].values, index=question_ids[j])

    for i in range(n_folds):
	    validate_dfs[i]['predicted_score'] = 0.
	    validate_dfs[i]['support'] = 0.

    print "Computing predicted scores."
    sys.stdout.flush()
	
    for i in range(1): #should be n_folds for full n_fold statistics
        pass
        recommender = Recommender(site_name,train_df_dicts[i])
        recommender.train(iterations = 1000, passes = 1)
        for j in range(len(validate_dfs[i])):
            pred_score, supp = recommender.predicted_score(validate_dfs[i].user_id.irow(j),validate_dfs[i][['title','question','tags']].irow(j))
            validate_dfs[i]['predicted_score'][j] = pred_score
            validate_dfs[i]['support'][j] = supp

    results = pd.DataFrame(data={'actual':validate_dfs[0]['score'], 'predicted':validate_dfs[0]['predicted_score'], 'support':validate_dfs[0]['support']})
    
    with open('data/'+site_name+"/one-fold-results.pkl","wb") as f:
        pkl.dump(results, f)

    end_time = time.time()
    print "Done in %.2fs!\n" % (end_time-start_time)
