import stackrecommender as ser
import numpy as np
import pandas as pd
from sklearn.cross_validation import KFold
import warnings
import pickle as pkl
import sys, time

warnings.filterwarnings("ignore")


def cv_recommender(k = 7, w = [1,1,1], num_topics = 20, iterations = 50, passes = 5):
    """
    Uses the n_fold train/test splits to validate with the given parameters. Outputs 
    a dataframe with columns 'actual' and 'predicted'.
    
    WARNING: super hacky!
    """
    
    # make a list of dataframes
    validate_dfs = range(n_folds)
    for j in range(n_folds):
        validate_dfs[j] = pd.DataFrame(data=questions[j], index=question_ids[j])
        validate_dfs[j]['user_id'] = pd.Series(data=users[j].values, index=question_ids[j])
        validate_dfs[j]['score'] = pd.Series(data=scores[j].values, index=question_ids[j])
        
    for i in range(n_folds):
        validate_dfs[i]['predicted_score'] = 0.
        validate_dfs[i]['support'] = 0.

    for i in range(n_folds):
        recommender = ser.Recommender(train_df_dicts[i])
        recommender.train(num_topics = num_topics, iterations = iterations, passes = passes)
        for j in range(len(validate_dfs[i])):
            pred_score, supp = recommender.predicted_score(validate_dfs[i].user_id.irow(j), 
                                                     validate_dfs[i][['title','question','tags']].irow(j),
                                                     k, w)
            validate_dfs[i]['predicted_score'][j] = pred_score
            validate_dfs[i]['support'][j] = supp
            
    results = pd.DataFrame(columns=['actual','predicted'])
    results.actual = pd.concat([vdf.score for vdf in validate_dfs])
    results.predicted = pd.concat([vdf.predicted_score for vdf in validate_dfs])
    
    return results


if __name__ == "__main__":

    start_time = time.time()

######### Setup #################

    # parameters
    n_folds = 5

    print "\n  === Grid Search via %d-Fold Cross Validation ===\n" % n_folds
    sys.stdout.flush()

    print "Generating dataframes."
    sys.stdout.flush()
    site = ser.stacksite("datascience.stackexchange.com")
    df_dict = site.generate_dfs()

    df_names = ['questions', 'comments']
    df_lengths = {name:len(site.df(name)) for name in df_names}

    # generate the train/test split index arrays
    folds = {name:KFold(df_lengths[name], n_folds=n_folds) for name in df_names}

    print "Splitting the questions."
    sys.stdout.flush()
    # generate the train/test dataframes
    train_dfs = {name:[] for name in df_names}
    test_dfs = {name:[] for name in df_names}
    for name in df_names:
        for test, train in folds[name]:
            train_dfs[name].append(site.df(name).ix[train])
            test_dfs[name].append(site.df(name).ix[test])

    print "Splitting the answers."
    sys.stdout.flush()
    # split answers_df according to the train/test split of questions_d
    train_dfs['answers'] = range(n_folds)
    test_dfs['answers'] = range(n_folds)
    answers_df = site.df('answers').copy()

    for j in range(n_folds):
        train_qids = set(train_dfs['questions'][j].index.unique()) # indices are already unique
        test_qids = set(test_dfs['questions'][j].index.unique()) # but df.index is mutable and not hashable
        train_dfs['answers'][j] = answers_df[answers_df.parent_id.isin(train_qids)]
        test_dfs['answers'][j] = answers_df[answers_df.parent_id.isin(test_qids)]

    # pack the training sets into dictionaries of dataframes for the recommender
    df_names.append('answers')
    train_df_dicts = range(n_folds)
    for j in range(n_folds):
        train_df_dicts[j] = {name:train_dfs[name][j] for name in df_names}
        train_df_dicts[j]['tags'] = site.df('tags')
        train_df_dicts[j]['users'] = site.df('users')

    # users and scores are lists of users/scores lists (one list for each fold)
    # question_ids is a list of lists of the corresponding question_ids
    # questions is a list of lists of questions (one list for each fold)

    print "Packing the test/train dataframes."
    sys.stdout.flush()

    users = [answers_df.user_id for answers_df in test_dfs['answers']]
    scores = [answers_df.score for answers_df in test_dfs['answers']]
    question_ids = [answers_df.parent_id for answers_df in test_dfs['answers']]
    questions = range(n_folds)
    for j in range(n_folds):
        questions[j] = [test_dfs['questions'][j][['title','question','tags']].ix[qid] for qid in question_ids[j]]
        
    # make a list of dataframes
    validate_dfs = range(n_folds)
    for j in range(n_folds):
        validate_dfs[j] = pd.DataFrame(data=questions[j], index=question_ids[j])
        validate_dfs[j]['user_id'] = pd.Series(data=users[j].values, index=question_ids[j])
        validate_dfs[j]['score'] = pd.Series(data=scores[j].values, index=question_ids[j])



########## Grid Search Starts Here ###############

    # search parameters (I'll search each block separately first)
    # block 1
    iterations = [10, 50, 100, 500, 1000]

    # block 2
    passes = [5, 10, 20]

    # block 3
    num_topics = [5, 10, 20, 100]

    # block 4
    k = [3, 7, 11]

    # block 5
    w = [[1,0,0], [0,1,0], [0,0,1], [1,1,0], [1,0,1], [0,1,1], [1,1,1]]


    output = {}

    # run the validation
    print "\nCross validating. Iterations: ",
    sys.stdout.flush()
    for its in iterations:
        print its,
        sys.stdout.flush()

        output['iterations = '+str(its)] = cv_recommender(iterations = its)
        with open('gridsearch_results.pkl','wb') as f:
            pkl.dump(output, f)

    print "\nCross validating. Passes: ",
    sys.stdout.flush()
    for ps in passes:
        print ps,
        sys.stdout.flush() 

        output['passes = '+str(ps)] = cv_recommender(passes = ps)
        with open('gridsearch_results.pkl','wb') as f:
            pkl.dump(output, f)

    print "\nCross validating. num_topics: ",
    sys.stdout.flush()
    for nt in num_topics:
        print nt, 
        sys.stdout.flush()

        output['num_topics = '+str(nt)] = cv_recommender(num_topics = nt)
        with open('gridsearch_results.pkl','wb') as f:
            pkl.dump(output, f)  

    print "\nCross validating. k: ",
    sys.stdout.flush()
    for pk in k:
        print pk,
        sys.stdout.flush() 

        output['k = '+str(pk)] = cv_recommender(k = pk)
        with open('gridsearch_results.pkl','wb') as f:
            pkl.dump(output, f)  

    print "\nCross validating. w: ",
    sys.stdout.flush()
    for pw in w:
        print pw,
        sys.stdout.flush() 

        output['w = '+str(pw)] = cv_recommender(w = pw)
        with open('gridsearch_results.pkl','wb') as f:
            pkl.dump(output, f)    

    end_time = time.time()

    print "\nDone in %.2fs. Results written to %s." % (end_time - start_time, 'gridsearch_results.pkl')