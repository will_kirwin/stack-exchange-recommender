{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Scoring Recommender Systems "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One major source of difficulty in building recommender systems is that the user-item ratings matrix is mostly empty, simply because most users haven't rated most items. But the matrix can't be treated as sparse, since a user might rate an item highly *if* s/he were to rate it. That means that treating empty entries as zero will usually be wrong.\n",
    "\n",
    "Because of the *emptiness* of the user-item ratings matrix, many of the usual scores and error functions used in regression and classification problems are not very useful. One score which is occasionally reported is the accuracy of the recommender system on the set of non-empty ratings in the test set. A more popular error function is the RMSE of the recommender on the non-empty ratings in the test set. This is, for example, what Netflix used.\n",
    "\n",
    "Let $r_{um}$ denote the rating a user `u` gives an item `m` (for us, the items will be questions, and ratings will be binned scores of answers to those questions). A recommender system makes a prediction $\\hat{r}_{um}$ for each user `u` and item `m`.\n",
    "\n",
    "The RMSE on the test set is \n",
    "\n",
    "$$\\text{RMSE} := \\sqrt{\\sum_{(u,m)\\in\\text{test}} \\frac{(r_{um} - \\hat{r}_{um})^2}{\\#|\\text{test}|}}.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The Latent Factor Model "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Denote the number of stars (binned score) user $u$ actually got on their answer to question $m$ by $r_{um}$, and the predicted score by $\\hat{r}_{um}$. Denote the mean stars of all answers to question $m$ by $\\bar{r}_m$ and the mean stars of all answer by user $u$ by $\\bar{r}_u.$ Denote the overall mean stars by $\\mu.$ The latent factor model is\n",
    "\n",
    "$$ \\hat{r}_{um} = \\mu + (\\bar{r}_u - \\mu) + (\\bar{r}_m - \\mu) + R_{um}, $$\n",
    "\n",
    "where $(\\bar{r}_u)$ and $(\\bar{r}_m)$ are the user and question biases (resp.), and $R_{um}$ is given by a low-rank matrix factorization. \n",
    "\n",
    "### Accounting for user/item bias.\n",
    "In more advanced recommender systems, the biases $\\bar{r}_u$ and $\\bar{r}_m$ are chosen to minimize \n",
    "\n",
    "$$ \\sum_{u} \\sum_{m\\text{ rated by }u} (r_{um} - \\mu - \\bar{r}_u - \\bar{r}_m)^2 + \\# \\bar{r}_m^2 + \\#\\bar{r}_u^2,$$\n",
    "\n",
    "where \\# are some regularization parameters. In our simplified model, we will just use\n",
    "\n",
    "$$\\bar{r}_u = \\frac{1}{\\#|\\text{items rated by }u|} \\sum_{m\\text{ rated by }u} r_{um}$$\n",
    "\n",
    "and\n",
    "\n",
    "$$\\bar{r}_m = \\frac{1}{\\#|\\text{users who rated }m|} \\sum_{m\\text{ rated by }u} r_{um}.$$\n",
    "\n",
    "### Low-rank factorization.\n",
    "The low-rank factorization is given explicitly by \n",
    "\n",
    "$$ R_{um} = \\theta_u^T \\gamma_m,$$\n",
    "\n",
    "where \n",
    "\n",
    "$$\\theta_u := (\\theta_u^1, ..., \\theta_u^N)$$\n",
    "\n",
    "is a vector of $N$ latent features describing user `u`, and \n",
    "\n",
    "$$\\gamma_m := (\\gamma_m^1, ..., \\gamma_m^N)$$\n",
    "\n",
    "is a vector of $N$ latent features describing question `m`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Stack Exchange Recommender"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Scores to Stars\n",
    "\n",
    "First, we logarithmically bin the scores into five classes: `0, 1, 2, 3, 4`. This helps for two reasons.\n",
    "\n",
    "1. Our recommender system looks like a standard Netflix-style recommender system, where a user rates an item on a scale of `0` - `4` (or `1` - `5`).\n",
    "\n",
    "2. There seems to be some bandwagon-effect in the scores that makes it very hard to predict the actual magnitude of large scores. (Popular question get more votes, which makes them more popular, so they get more votes, etc...)\n",
    "\n",
    "The bins are $(-\\infty, 0, 2, 7, 17, \\infty)$, although because of the finite range of scores, this is effectively a translated logarithmic scale."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Choosing the Model\n",
    "\n",
    "To determine various parameters and models, we used `worldbuilding.stackexchange.com`. Some statistics of the site:\n",
    "\n",
    "    users: 4728\n",
    "    questions: 1321\n",
    "    answers: 6344\n",
    "    \n",
    "The user-item ratings matrix has `6.2M` entries, of which `6344` are nonzero: that is, we are trying to model a matrix which is `99.9%` empty.\n",
    "\n",
    "The scores range between `-5` and `72`, and after binning are distributed as below.\n",
    "\n",
    "<img width=720 src='stars.png' />"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For speed, our latent factor model is implemented in Apache Spark <img width = 100 src='spark-logo.png' />"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Number of features $N$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The number of features to use in the latent-factor model is a parameter."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img width=720 src=\"LF-model.png\" />"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Modeling the residuals."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is *very* easy to do worse than just predicting the overall mean (the trivial model). The laten factor model does better. Improving on that is even harder."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img width=720 src=\"model-comps.png\" />"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img width=720 src=\"model-comps2.png\" />"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<style>\n",
       "    @font-face {\n",
       "        font-family: \"Computer Modern\";\n",
       "        src: url('http://9dbb143991406a7c655e-aa5fcb0a5a4ec34cff238a2d56ca4144.r56.cf5.rackcdn.com/cmunss.otf');\n",
       "    }\n",
       "    @font-face {\n",
       "        font-family: \"Computer Modern\";\n",
       "        font-weight: bold;\n",
       "        src: url('http://9dbb143991406a7c655e-aa5fcb0a5a4ec34cff238a2d56ca4144.r56.cf5.rackcdn.com/cmunsx.otf');\n",
       "    }\n",
       "    @font-face {\n",
       "        font-family: \"Computer Modern\";\n",
       "        font-style: oblique;\n",
       "        src: url('http://9dbb143991406a7c655e-aa5fcb0a5a4ec34cff238a2d56ca4144.r56.cf5.rackcdn.com/cmunsi.otf');\n",
       "    }\n",
       "    @font-face {\n",
       "        font-family: \"Computer Modern\";\n",
       "        font-weight: bold;\n",
       "        font-style: oblique;\n",
       "        src: url('http://9dbb143991406a7c655e-aa5fcb0a5a4ec34cff238a2d56ca4144.r56.cf5.rackcdn.com/cmunso.otf');\n",
       "    }\n",
       "    div.cell{\n",
       "        width:800px;\n",
       "        margin-left:16% !important;\n",
       "        margin-right:auto;\n",
       "    }\n",
       "    h1 {\n",
       "        font-family: Helvetica, serif;\n",
       "    }\n",
       "    h4{\n",
       "        margin-top:12px;\n",
       "        margin-bottom: 3px;\n",
       "       }\n",
       "    div.text_cell_render{\n",
       "        font-family: Computer Modern, \"Helvetica Neue\", Arial, Helvetica, Geneva, sans-serif;\n",
       "        line-height: 145%;\n",
       "        font-size: 130%;\n",
       "        width:800px;\n",
       "        margin-left:auto;\n",
       "        margin-right:auto;\n",
       "    }\n",
       "    .CodeMirror{\n",
       "            font-family: \"Source Code Pro\", source-code-pro,Consolas, monospace;\n",
       "    }\n",
       "    .prompt{\n",
       "        display: None;\n",
       "    }\n",
       "    .text_cell_render h5 {\n",
       "        font-weight: 300;\n",
       "        font-size: 22pt;\n",
       "        color: #4057A1;\n",
       "        font-style: italic;\n",
       "        margin-bottom: .5em;\n",
       "        margin-top: 0.5em;\n",
       "        display: block;\n",
       "    }\n",
       "    \n",
       "    .warning{\n",
       "        color: rgb( 240, 20, 20 )\n",
       "        }  \n",
       "</style>\n",
       "<script>\n",
       "    MathJax.Hub.Config({\n",
       "                        TeX: {\n",
       "                           extensions: [\"AMSmath.js\"]\n",
       "                           },\n",
       "                tex2jax: {\n",
       "                    inlineMath: [ ['$','$'], [\"\\\\(\",\"\\\\)\"] ],\n",
       "                    displayMath: [ ['$$','$$'], [\"\\\\[\",\"\\\\]\"] ]\n",
       "                },\n",
       "                displayAlign: 'center', // Change this to 'center' to center equations.\n",
       "                \"HTML-CSS\": {\n",
       "                    styles: {'.MathJax_Display': {\"margin\": 4}}\n",
       "                }\n",
       "        });\n",
       "</script>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from IPython.core.display import HTML\n",
    "def css_styling():\n",
    "    styles = open(\"custom.css\", \"r\").read() #or edit path to custom.css\n",
    "    return HTML(styles)\n",
    "css_styling()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
